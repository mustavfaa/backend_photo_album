from django.db import models
from django.utils.html import escape
class Car(models.Model):
    name = models.CharField(max_length=255)
    price = models.DecimalField(max_digits=5, decimal_places=2)
    photo = models.ImageField(upload_to='cars')

    def __str__(self):
        return self.name

class PhotoAlbum(models.Model):
    name = models.CharField(max_length=255)
    def __str__(self):
        return self.name
    
class Photo(models.Model):
    photo = models.ImageField(upload_to='photo')
    def __str__(self):
        return self.photo.path  
      
    
class PhotoGroup(models.Model):
    name = models.ForeignKey(PhotoAlbum, on_delete=models.CASCADE, related_name='related_%(app_label)s_%(class)s_name' )
    photo = models.ForeignKey(Photo, on_delete=models.CASCADE, related_name='related_%(app_label)s_%(class)s_photo' )

    # def __str__(self):
    #     return self.photo.path
 
class CategoryName(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.ForeignKey(CategoryName, on_delete=models.CASCADE, related_name='related_%(app_label)s_%(class)s_name' )
    photoAlbum = models.ForeignKey(PhotoAlbum, on_delete=models.CASCADE, related_name='related_%(app_label)s_%(class)s_photoAlbum' )

    def __str__(self):
        return self.name


class Number(models.Model):
    number = models.IntegerField()

    def __str__(self):
        return str(self.number)