from rest_framework import routers, serializers, viewsets
from django.contrib.auth.models import User, Group
from .models import PhotoAlbum, Photo, PhotoGroup, CategoryName, Category, Number

# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'is_staff']

class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']


class PhotoAlbumSerializer(serializers.ModelSerializer):
    class Meta:
        model = PhotoAlbum
        fields = ['id', 'name']

    # def to_representation(self, instance):
    #     ret = super().to_representation(instance)
    #     photo_groups = instance.related_quickstart_photogroup_name.all()
    #
    #     photos = []
    #     for item in photo_groups:
    #         photos.append(item.photo)
    #
    #     photosSerialised = PhotoSerializer(photos, many=True).data
    #     ret['photo'] = photosSerialised
    #     return ret

class PhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Photo
        fields = ['id', 'photo']


class PhotoGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = PhotoGroup
        fields = ['id',]

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        # name_data =
        ret['name'] = PhotoAlbumSerializer(instance.name).data # name_data['name']
        # photo_data =
        ret['photo'] = PhotoSerializer(instance.photo).data # photo_data['photo']
        return ret

class CategoryNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = CategoryName
        fields = ['id', 'name']

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['name', 'photoAlbum']


class NumberSerializer(serializers.ModelSerializer):
    class Meta:
        model = Number
        fields = ['id','number']

    # def to_representation(self, instance):
    #     number_model = super().to_representation(instance)
    #     number_model.objects.create()