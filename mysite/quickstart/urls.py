from django.urls import path, include
from rest_framework import routers
from .views import PhotoViewSet, PhotoAlbumViewSet,   PhotoGroupViewSet, CategoryNameViewSet, CategoryNameViewSet, NumberViewSet

router = routers.SimpleRouter()
router.register(r'photo', PhotoViewSet)
router.register(r'photo-album', PhotoAlbumViewSet)
router.register(r'photo-group', PhotoGroupViewSet)
router.register(r'category', CategoryNameViewSet)
router.register(r'category-name', CategoryNameViewSet)
router.register(r'number', NumberViewSet)

urlpatterns = router.urls