from django.contrib import admin
from .models import Car, PhotoAlbum, Photo, PhotoGroup, CategoryName, Category, Number

 
@admin.register(Car)
class CarModelAdmin(admin.ModelAdmin):
    # autocomplete_fields = ['organization']
    list_display = ('name', 'price','photo',  )
# Register your models here.


@admin.register(PhotoAlbum)
class PhotoAlbumModelAdmin(admin.ModelAdmin):
    # autocomplete_fields = ['organization']
    list_display = (  'name', )

@admin.register(Photo)
class PhotoModelAdmin(admin.ModelAdmin):
    # autocomplete_fields = ['organization']
    list_display = ( 'photo', )

@admin.register(PhotoGroup)
class PhotoGroupModelAdmin(admin.ModelAdmin):
    # autocomplete_fields = ['organization']
    list_display = ( 'name', 'photo' )


@admin.register(CategoryName)
class CategoryNameModelAdmin(admin.ModelAdmin):
    # autocomplete_fields = ['organization']
    list_display = ('name',  )

@admin.register(Category)
class CategoryModelAdmin(admin.ModelAdmin):
    # autocomplete_fields = ['organization']
    list_display = ( 'name', 'photoAlbum' )
    

@admin.register(Number)
class NumberModelAdmin(admin.ModelAdmin):
    # autocomplete_fields = ['organization']
    list_display = ( 'id', 'number', )