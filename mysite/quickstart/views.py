from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.decorators import action
from rest_framework import status

from .models import PhotoAlbum, Photo, PhotoGroup, CategoryName, Category, Number
from .serialisers import UserSerializer, GroupSerializer, PhotoAlbumSerializer, PhotoSerializer, PhotoGroupSerializer, CategoryNameSerializer, CategorySerializer, NumberSerializer
from rest_framework.response import Response
import io
from rest_framework.parsers import JSONParser
class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [ ]


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [] # [permissions.IsAuthenticated]



class PhotoAlbumViewSet(viewsets.ModelViewSet):
 
    queryset = PhotoAlbum.objects.all()
    serializer_class = PhotoAlbumSerializer
    permission_classes = [] # [permissions.IsAuthenticated]

    @action(methods=["get"], detail=False, url_name="get_test",url_path="get_test")
    def get_test(self, request, pk=None):
        id = request.query_params.get('id')
        photo_groups = PhotoGroup.objects.filter(name_id=id)
        photo_album = photo_groups.first().name
        photos = []
        for item in photo_groups:
            photos.append(item.photo)

        photosSerialised = PhotoSerializer(photos, many=True).data
        nameSerialised = PhotoAlbumSerializer(photo_album).data
        ret = {}
        ret['name'] = nameSerialised
        ret['photo'] = photosSerialised
        return Response(ret)


class PhotoViewSet(viewsets.ModelViewSet):
 
    queryset = Photo.objects.all()
    serializer_class = PhotoSerializer
    permission_classes = [] # [permissions.IsAuthenticated]

class PhotoGroupViewSet(viewsets.ModelViewSet):
 
    queryset = PhotoGroup.objects.all()
    serializer_class = PhotoGroupSerializer
    permission_classes = []
     
class CategoryNameViewSet(viewsets.ModelViewSet):
    queryset = CategoryName.objects.all()
    serializer_class = CategoryNameSerializer
    permission_classes = []

class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = []

class NumberViewSet(viewsets.ModelViewSet):
    queryset = Number.objects.all()
    serializer_class = NumberSerializer
    permission_classes = []

    @action(methods=['post'], detail=False, url_path='setnumber')
    def save_number(self, request, pk=None):
        serializer = NumberSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
